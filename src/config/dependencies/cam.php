<?php

declare(strict_types=1);

use GuzzleHttp\Client;
use Paneric\ComponentModuleApc\Responder\Responder;
use Paneric\HttpClient\HttpClientAdapter;
use Paneric\Interfaces\Responder\ResponderInterface;
use Paneric\Interfaces\HttpClient\HttpClientInterface;
use Paneric\Logger\HttpClientLogger;
use Psr\Container\ContainerInterface;
use Twig\Environment as Twig;

return [
    HttpClientInterface::class => static function (ContainerInterface $c): HttpClientAdapter {
        return new HttpClientAdapter(
            $c->get(Client::class),
            $c->get(HttpClientLogger::class),
        );
    },

    ResponderInterface::class => static function (ContainerInterface $c): Responder {
        return new Responder(
            $c->get(Twig::class),
        );
    },
];
