<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Responder;

use Paneric\Interfaces\Responder\ResponderInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class Responder implements ResponderInterface
{
    public function __construct(readonly Twig $twig)
    {
    }

    public function __invoke(Response $response, array $data = [], ...$params): Response
    {
        return $this->htmlResponse($response, $data, $params[0] ?? null);
    }

    protected function htmlResponse(
        Response $response,
        array $data,
        ?string $template = null
    ): Response {
        if ($template === null) {
            return $response
                ->withHeader('Location', $data['redirect_url'])
                ->withStatus($data['status']);
        }

        try {
            $response->withHeader('Content-Type', 'text/html;charset=utf-8');
            $response = $response->withStatus($data['status']);
            $body = $this->twig->render($template, $data);
            $response->getBody()->write($body);
        } catch (LoaderError | RuntimeError | SyntaxError $e) {
            echo sprintf(
                '%s%s%s%s',
                $e->getFile() . "\n",
                $e->getLine() . "\n",
                $e->getMessage() . "\n",
                $e->getTraceAsString() . "\n"
            );
        }

        return $response;
    }
}
