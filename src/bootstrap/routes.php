<?php

declare(strict_types=1);

use Paneric\ComponentModuleApc\Action\CreateApcAction;
use Paneric\ComponentModuleApc\Action\CreateMultipleApcAction;
use Paneric\ComponentModuleApc\Action\GetAllApcAction;
use Paneric\ComponentModuleApc\Action\GetAllByApcAction;
use Paneric\ComponentModuleApc\Action\GetAllPaginatedApcAction;
use Paneric\ComponentModuleApc\Action\GetAllByExtendedApcAction;
use Paneric\ComponentModuleApc\Action\GetOneByIdApcAction;
use Paneric\ComponentModuleApc\Action\GetOneByApcAction;
use Paneric\ComponentModuleApc\Controller\ApcModuleController;
use Paneric\ComponentModuleApc\Service\GetDeleteApcService;
use Paneric\ComponentModuleApc\Service\GetDeleteMultipleApcService;
use Paneric\ComponentModuleApc\Service\GetUpdateApcService;
use Paneric\ComponentModuleApc\Service\GetUpdateMultipleApcService;
use Paneric\Middleware\CSRFMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($slim, $container, $module, $proxyPrefix)) {
    $routePrefix = '/' . $module;
    $routeNamePrefix = $module;

    if (!empty($proxyPrefix)) {
        $routePrefix = $proxyPrefix  . '/' . $module;
        $routeNamePrefix = str_replace('/', '', $proxyPrefix) . '.' . $module;
        $routeNamePrefix = str_replace('-apc', '', $routeNamePrefix);
    }

    try {
        $slim->map(['GET', 'POST'], $routePrefix . '/add', function (Request $request, Response $response) {
            return $this->get(ApcModuleController::class)->create(
                $request,
                $response,
                $this->get(CreateApcAction::class)
            );
        })->setName($routeNamePrefix . '.add')
            ->add(CSRFMiddleware::class);

        $slim->map(['GET', 'POST'], $routePrefix . 's/add', function (Request $request, Response $response) {
            return $this->get(ApcModuleController::class)->createMultiple(
                $request,
                $response,
                $this->get(CreateMultipleApcAction::class)
            );
        })->setName($routeNamePrefix . 's.add')
            ->add(CSRFMiddleware::class);


        $slim->map(['GET', 'POST'], $routePrefix . '/remove/{id}', function (
            Request $request,
            Response $response,
            array $args
        ) {
            return $this->get(ApcModuleController::class)->delete(
                $request,
                $response,
                $this->get(GetDeleteApcService::class),
                $args['id']
            );
        })->setName($routeNamePrefix . '.remove')
            ->add(CSRFMiddleware::class);

        $slim->map(['GET', 'POST'], $routePrefix . 's/remove[/{page}]', function (
            Request $request,
            Response $response,
            array $args
        ) {
            return $this->get(ApcModuleController::class)->deleteMultiple(
                $request,
                $response,
                $this->get(GetDeleteMultipleApcService::class),
                $args['page'] ?? '1'
            );
        })->setName($routeNamePrefix . 's.remove')
            ->add(CSRFMiddleware::class);


        $slim->get($routePrefix . 's/get', function (Request $request, Response $response) {
            return $this->get(ApcModuleController::class)->getAll(
                $request,
                $response,
                $this->get(GetAllApcAction::class)
            );
        })->setName($routeNamePrefix . 's.get');

        $slim->get($routePrefix . 's/get/{field}/{value}', function (
            Request $request,
            Response $response,
            array $args
        ) {
            return $this->get(ApcModuleController::class)->getAllBy(
                $request,
                $response,
                $this->get(GetAllByApcAction::class),
                $args['field'],
                $args['value']
            );
        })->setName($routeNamePrefix . 's.get-by');

        // /auth-apc/actions/get-ext?find=ref&fvalue[]=add&fvalue[]=remove
        // /auth-apc/actions/get-ext?find[]=ref&find[]=id&fvalue[]=add&fvalue[]=1
        $slim->get($routePrefix . 's/get-ext', function (Request $request, Response $response) {
            return $this->get(ApcModuleController::class)->getAllByExtended(
                $request,
                $response,
                $this->get(GetAllByExtendedApcAction::class)
            );
        })->setName($routeNamePrefix . 's.get-ext');

        $slim->get($routePrefix . 's/get-paginated[/{page}]', function (
            Request $request,
            Response $response,
            array $args
        ) {
            return $this->get(ApcModuleController::class)->getAllPaginated(
                $request,
                $response,
                $this->get(GetAllPaginatedApcAction::class),
                $args['page'] ?? '1'
            );
        })->setName($routeNamePrefix . 's.get-paginated');


        $slim->get($routePrefix . '/get/{field}/{value}', function (
            Request $request,
            Response $response,
            array $args
        ) {
            return $this->get(ApcModuleController::class)->getOneBy(
                $request,
                $response,
                $this->get(GetOneByApcAction::class),
                $args['field'],
                $args['value']
            );
        })->setName($routeNamePrefix . '.get-by');

        $slim->get($routePrefix . '/get/{id}', function (
            Request $request,
            Response $response,
            array $args
        ) {
            return $this->get(ApcModuleController::class)->getOneById(
                $request,
                $response,
                $this->get(GetOneByIdApcAction::class),
                $args['id']
            );
        })->setName($routeNamePrefix . '.get-by-id');


        $slim->map(['GET', 'POST'], $routePrefix . '/edit/{id}', function (
            Request $request,
            Response $response,
            array $args
        ) {
            return $this->get(ApcModuleController::class)->update(
                $request,
                $response,
                $this->get(GetUpdateApcService::class),
                $args['id']
            );
        })->setName($routeNamePrefix . '.edit')
            ->add(CSRFMiddleware::class);

        $slim->map(['GET', 'POST'], $routePrefix . 's/edit[/{page}]', function (
            Request $request,
            Response $response,
            array $args
        ) {
            return $this->get(ApcModuleController::class)->updateMultiple(
                $request,
                $response,
                $this->get(GetUpdateMultipleApcService::class),
                $args['page'] ?? '1'
            );
        })->setName($routeNamePrefix . 's.edit')
            ->add(CSRFMiddleware::class);
    } catch (Exception $e) {
    }
}
