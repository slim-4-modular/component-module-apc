<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Action;

use Paneric\ComponentModuleApc\Interfaces\Action\CreateApcActionInterface;

class CreateApcAction extends AbstractCreateApcAction implements CreateApcActionInterface
{
}
