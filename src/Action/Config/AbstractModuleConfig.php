<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Action\Config;

abstract class AbstractModuleConfig
{
    use ModuleConfigTrait;

    public function __construct(private readonly string $apiBaseUrl)
    {
    }

    public function getApiBaseUrl(): string
    {
        return $this->apiBaseUrl;
    }
}
