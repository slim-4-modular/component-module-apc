<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Action\Config;

trait ModuleConfigTrait
{
    public function create(
        string $proxyPrefix,
        ?array $attributes,
        ?string $token = null,
        ?string $local = null
    ): array {
        return [
            'redirect_url' => sprintf(
                '%s/%ss/get-paginated?local=%s',
                $proxyPrefix,
                $this->getModuleName(),
                $local
            ),
            'method' => 'POST',
            'uri_suffix' => '/add',
            'options' => $this->getOptions($token, null, $local, $attributes),
        ];
    }

    public function createMultiple(
        string $proxyPrefix,
        ?array $attributes,
        ?string $token = null,
        ?string $local = null
    ): array {
        return [
            'redirect_url' => sprintf(
                '%s/%ss/get-paginated?local=%s',
                $proxyPrefix,
                $this->getModuleName(),
                $local
            ),
            'method' => 'POST',
            'uri_suffix' => 's/add',
            'options' => $this->getOptions($token, null, $local, $attributes),
        ];
    }

    public function delete(
        string $proxyPrefix,
        ?string $token = null,
        ?string $local = null
    ): array {
        return [
            'redirect_url' => sprintf(
                '%s/%ss/get-paginated?local=%s',
                $proxyPrefix,
                $this->getModuleName(),
                $local
            ),
            'method' => 'DELETE',
            'uri_suffix' => '/remove',
            'options' => $this->getOptions($token, null, $local),
        ];
    }

    public function deleteMultiple(
        string $proxyPrefix,
        ?array $attributes,
        ?string $token = null,
        ?string $local = null
    ): array {
        return [
            'redirect_url' => sprintf(
                '%s/%ss/get-paginated?local=%s',
                $proxyPrefix,
                $this->getModuleName(),
                $local
            ),
            'method' => 'DELETE',
            'uri_suffix' => 's/remove',
            'options' => $this->getOptions($token, null, $local, $attributes),
        ];
    }

    public function getAll(?string $token = null, ?array $queryParams = null, ?string $local = null): array
    {
        return [
            'method' => 'GET',
            'uri_suffix' => 's/get',
            'options' => $this->getOptions($token, $queryParams, $local),
        ];
    }

    public function getAllBy(?string $token = null, ?array $queryParams = null, ?string $local = null): array
    {
        return [
            'method' => 'GET',
            'uri_suffix' => 's/get',
            'options' => $this->getOptions($token, $queryParams, $local),
        ];
    }

    public function getAllByExtended(?string $token = null, ?array $queryParams = null, ?string $local = null): array
    {
        return [
            'method' => 'GET',
            'uri_suffix' => 's/get-ext',
            'options' => $this->getOptions($token, $queryParams, $local),
        ];
    }

    public function getAllPaginated(?string $token = null, ?array $queryParams = null, ?string $local = null): array
    {
        return [
            'method' => 'GET',
            'uri_suffix' => 's/get-paginated',
            'options' => $this->getOptions($token, $queryParams, $local),
        ];
    }

    public function getOneBy(?string $token = null, ?string $local = null): array
    {
        return [
            'method' => 'GET',
            'uri_suffix' => '/get',
            'options' => $this->getOptions($token, null, $local),
        ];
    }

    public function getOneById(?string $token = null, ?string $local = null): array
    {
        return [
            'method' => 'GET',
            'uri_suffix' => '/get',
            'options' => $this->getOptions($token, null, $local),
        ];
    }

    public function update(
        string $proxyPrefix,
        ?array $attributes,
        ?string $token = null,
        ?string $local = null
    ): array {
        return [
            'redirect_url' => sprintf(
                '%s/%ss/get-paginated?local=%s',
                $proxyPrefix,
                $this->getModuleName(),
                $local
            ),
            'method' => 'PUT',
            'uri_suffix' => '/edit',
            'options' => $this->getOptions($token, null, $local, $attributes),
        ];
    }

    public function updateMultiple(
        string $proxyPrefix,
        ?array $attributes,
        ?string $token = null,
        ?string $local = null
    ): array {
        return [
            'redirect_url' => sprintf(
                '%s/%ss/get-paginated?local=%s',
                $proxyPrefix,
                $this->getModuleName(),
                $local
            ),
            'method' => 'PUT',
            'uri_suffix' => 's/edit',
            'options' => $this->getOptions($token, null, $local, $attributes),
        ];
    }

    protected function getOptions(
        ?string $token = null,
        ?array $queryParams = null,
        ?string $local = null,
        ?array $attributes = null
    ): array {
        $options = [
            'headers' => [
                'Content-Type' => 'application/json;charset=utf-8',
                'Authorization' => ' Bearer ' . $token,
            ],
            'debug' => false,
            'query' => array_merge($queryParams ?? [], ['local' =>  $local]),
        ];

        if ($attributes !== null) {
            $options = array_merge($options, ['json' => $attributes]);
        }

        return $options;
    }
}
