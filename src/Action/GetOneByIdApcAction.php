<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Action;

use Paneric\ComponentModuleApc\Interfaces\Action\GetOneByIdApcActionInterface;
use Paneric\ComponentModuleApc\Interfaces\ModuleConfigInterface;
use Paneric\Interfaces\HttpClient\HttpClientInterface;

class GetOneByIdApcAction implements GetOneByIdApcActionInterface
{
    public function __construct(
        protected HttpClientInterface $httpClient,
        protected ModuleConfigInterface $config
    ) {
    }

    public function __invoke(string $id, ?string $token = null, ?string $local = null): array
    {
        $config = $this->config->getOneById($token, $local);

        $data = $this->httpClient->getJsonResponse(
            $config['method'],
            sprintf(
                '%s/%s%s/%s',
                $this->config->getApiBaseUrl(),
                $this->config->getModuleName(),
                $config['uri_suffix'],
                $id
            ),
            $config['options']
        );

        return array_merge($data, ['module_name' => $this->config->getModuleName()]);
    }
}
