<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Action;

use Paneric\ComponentModuleApc\Action\Traits\DeleteUpdateMultipleApcActionTrait;
use Paneric\ComponentModuleApc\Interfaces\Action\DeleteMultipleApcActionInterface;
use Paneric\ComponentModuleApc\Interfaces\ModuleConfigInterface;
use Paneric\Interfaces\HttpClient\HttpClientInterface;

class DeleteMultipleApcAction implements DeleteMultipleApcActionInterface
{
    use DeleteUpdateMultipleApcActionTrait;

    public function __construct(
        protected HttpClientInterface $httpClient,
        protected ModuleConfigInterface $config
    ) {
    }

    public function __invoke(
        string $proxyPrefix,
        ?array $attributes = null,
        ?string $token = null,
        ?string $local = null
    ): ?array {
        return $this->invoke(
            $this->config->deleteMultiple($proxyPrefix, $attributes, $token, $local)
        );
    }
}
