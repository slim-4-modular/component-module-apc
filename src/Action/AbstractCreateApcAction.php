<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Action;

use Paneric\ComponentModuleApc\Interfaces\ModuleConfigInterface;
use Paneric\Interfaces\HttpClient\HttpClientInterface;

class AbstractCreateApcAction
{
    public function __construct(
        protected HttpClientInterface $httpClient,
        protected ModuleConfigInterface $config
    ) {
    }

    public function __invoke(
        string $method,
        string $configMethod,
        string $proxyPrefix,
        ?array $attributes = null,
        ?string $token = null,
        ?string $local = null
    ): ?array {
        if ($method === 'POST') {
            $config = $this->config->{$configMethod}($proxyPrefix, $attributes, $token, $local);

            $data = $this->httpClient->getJsonResponse(
                $config['method'],
                sprintf(
                    '%s/%s%s',
                    $this->config->getApiBaseUrl(),
                    $this->config->getModuleName(),
                    $config['uri_suffix']
                ),
                $config['options']
            );

            if ($data['status'] === 201) {
                $data['status'] = 303;
                $data['redirect_url'] = $config['redirect_url'];
            }

            return array_merge($data, ['module_name' => $this->config->getModuleName()]);
        }

        return [
            'status' => 200,
            'body' => [],
            'module_name' => $this->config->getModuleName(),
        ];
    }
}
