<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Action;

use Paneric\ComponentModuleApc\Interfaces\Action\CreateMultipleApcActionInterface;

class CreateMultipleApcAction extends AbstractCreateApcAction implements CreateMultipleApcActionInterface
{
}
