<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Action\Traits;

trait DeleteUpdateMultipleApcActionTrait
{
    protected function invoke(array $config): array
    {
        $data = $this->httpClient->getJsonResponse(
            $config['method'],
            sprintf(
                '%s/%s%s',
                $this->config->getApiBaseUrl(),
                $this->config->getModuleName(),
                $config['uri_suffix']
            ),
            $config['options']
        );

        if ($data['status'] === 200) {
            $data['status'] = 303;
            $data['redirect_url'] = $config['redirect_url'];
        }

        return array_merge($data, ['module_name' => $this->config->getModuleName()]);
    }
}
