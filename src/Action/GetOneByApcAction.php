<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Action;

use Paneric\ComponentModuleApc\Interfaces\Action\GetOneByApcActionInterface;
use Paneric\ComponentModuleApc\Interfaces\ModuleConfigInterface;
use Paneric\Interfaces\HttpClient\HttpClientInterface;

class GetOneByApcAction implements GetOneByApcActionInterface
{
    public function __construct(
        protected HttpClientInterface $httpClient,
        protected ModuleConfigInterface $config
    ) {
    }

    public function __invoke(string $field, string $value, ?string $token = null, ?string $local = null): array
    {
        $config = $this->config->getOneBy($token, $local);

        $data = $this->httpClient->getJsonResponse(
            $config['method'],
            sprintf(
                '%s/%s%s/%s/%s',
                $this->config->getApiBaseUrl(),
                $this->config->getModuleName(),
                $config['uri_suffix'],
                $field,
                $value
            ),
            $config['options']
        );

        return array_merge($data, ['module_name' => $this->config->getModuleName()]);
    }
}
