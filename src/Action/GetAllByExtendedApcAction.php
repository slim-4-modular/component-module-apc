<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Action;

use Paneric\ComponentModuleApc\Interfaces\Action\GetAllByExtendedApcActionInterface;
use Paneric\ComponentModuleApc\Interfaces\ModuleConfigInterface;
use Paneric\Interfaces\HttpClient\HttpClientInterface;

class GetAllByExtendedApcAction implements GetAllByExtendedApcActionInterface
{
    public function __construct(
        protected HttpClientInterface $httpClient,
        protected ModuleConfigInterface $config
    ) {
    }

    public function __invoke(?string $token = null, ?array $queryParams = null, ?string $local = null): array
    {
        $config = $this->config->getAllByExtended($token, $queryParams, $local);

        $data = $this->httpClient->getJsonResponse(
            $config['method'],
            sprintf(
                '%s/%s%s',
                $this->config->getApiBaseUrl(),
                $this->config->getModuleName(),
                $config['uri_suffix']
            ),
            $config['options']
        );

        return array_merge($data, ['module_name' => $this->config->getModuleName()]);
    }
}
