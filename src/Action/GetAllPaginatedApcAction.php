<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Action;

use Paneric\ComponentModuleApc\Interfaces\Action\GetAllPaginatedApcActionInterface;
use Paneric\ComponentModuleApc\Interfaces\ModuleConfigInterface;
use Paneric\Interfaces\HttpClient\HttpClientInterface;

class GetAllPaginatedApcAction implements GetAllPaginatedApcActionInterface
{
    public function __construct(
        protected HttpClientInterface $httpClient,
        protected ModuleConfigInterface $config
    ) {
    }

    public function __invoke(
        string $page,
        ?string $token = null,
        ?array $queryParams = null,
        ?string $local = null
    ): array {
        $config = $this->config->getAllPaginated($token, $queryParams, $local);

        $data = $this->httpClient->getJsonResponse(
            $config['method'],
            sprintf(
                '%s/%s%s/%s',
                $this->config->getApiBaseUrl(),
                $this->config->getModuleName(),
                $config['uri_suffix'],
                $page
            ),
            $config['options']
        );

        return array_merge($data, ['module_name' => $this->config->getModuleName()]);
    }
}
