<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Action;

use Paneric\ComponentModuleApc\Action\Traits\DeleteUpdateApcActionTrait;
use Paneric\ComponentModuleApc\Interfaces\Action\UpdateApcActionInterface;
use Paneric\ComponentModuleApc\Interfaces\ModuleConfigInterface;
use Paneric\Interfaces\HttpClient\HttpClientInterface;

class UpdateApcAction implements UpdateApcActionInterface
{
    use DeleteUpdateApcActionTrait;

    public function __construct(
        protected HttpClientInterface $httpClient,
        protected ModuleConfigInterface $config
    ) {
    }

    public function __invoke(
        string $id,
        string $proxyPrefix,
        ?array $attributes = null,
        ?string $token = null,
        ?string $local = null
    ): ?array {
        return $this->invoke(
            $id,
            $this->config->update($proxyPrefix, $attributes, $token, $local)
        );
    }
}
