<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Action;

use Paneric\ComponentModuleApc\Action\Traits\DeleteUpdateApcActionTrait;
use Paneric\ComponentModuleApc\Interfaces\Action\DeleteApcActionInterface;
use Paneric\ComponentModuleApc\Interfaces\ModuleConfigInterface;
use Paneric\Interfaces\HttpClient\HttpClientInterface;

class DeleteApcAction implements DeleteApcActionInterface
{
    use DeleteUpdateApcActionTrait;

    public function __construct(
        protected HttpClientInterface $httpClient,
        protected ModuleConfigInterface $config
    ) {
    }

    public function __invoke(
        $id,
        string $proxyPrefix,
        ?string $token = null,
        ?string $local = null
    ): ?array {

        return $this->invoke(
            $id,
            $this->config->delete($proxyPrefix, $token, $local)
        );
    }
}
