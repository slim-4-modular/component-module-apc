<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Service;

use Paneric\ComponentModuleApc\Action\DeleteMultipleApcAction;
use Paneric\ComponentModuleApc\Action\GetAllPaginatedApcAction;
use Paneric\ComponentModuleApc\Interfaces\Service\GetDeleteMultipleApcServiceInterface;

class GetDeleteMultipleApcService implements GetDeleteMultipleApcServiceInterface
{
    public function __construct(
        protected GetAllPaginatedApcAction $getAllPaginatedApcAction,
        protected DeleteMultipleApcAction $deleteMultipleApcAction
    ) {
    }

    public function __invoke(
        string $page,
        string $method,
        string $proxyPrefix,
        ?array $queryParams = null,
        ?array $attributes = null,
        ?string $token = null,
        ?string $local = null
    ): ?array {
        if ($method === 'GET') {
            return ($this->getAllPaginatedApcAction)($page, $token, $queryParams, $local);
        }

        $data = ($this->deleteMultipleApcAction)($proxyPrefix, $attributes, $token, $local);

        if ($data['status'] === 303) {
            return $data;
        }

        return array_merge(
            $data,
            ['page_content' => ($this->getAllPaginatedApcAction)($page, $token, $queryParams, $local)]
        );
    }
}
