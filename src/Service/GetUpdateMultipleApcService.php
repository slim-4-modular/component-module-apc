<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Service;

use Paneric\ComponentModuleApc\Action\GetAllPaginatedApcAction;
use Paneric\ComponentModuleApc\Action\UpdateMultipleApcAction;
use Paneric\ComponentModuleApc\Interfaces\Service\GetUpdateMultipleApcServiceInterface;

class GetUpdateMultipleApcService implements GetUpdateMultipleApcServiceInterface
{
    public function __construct(
        protected GetAllPaginatedApcAction $getAllPaginatedApcAction,
        protected UpdateMultipleApcAction $updateMultipleApcAction
    ) {
    }

    public function __invoke(
        string $page,
        string $method,
        string $proxyPrefix,
        ?array $queryParams = null,
        ?array $attributes = null,
        ?string $token = null,
        ?string $local = null
    ): ?array {
        if ($method === 'GET') {
            return ($this->getAllPaginatedApcAction)($page, $token, $queryParams, $local);
        }

        $data = ($this->updateMultipleApcAction)($proxyPrefix, $attributes, $token, $local);

        if ($data['status'] === 303) {
            return $data;
        }

        return array_merge(
            $data,
            ['page_content' => ($this->getAllPaginatedApcAction)($page, $token, $queryParams, $local)]
        );
    }
}
