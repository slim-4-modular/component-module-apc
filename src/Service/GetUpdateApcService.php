<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Service;

use Paneric\ComponentModuleApc\Action\GetOneByIdApcAction;
use Paneric\ComponentModuleApc\Action\UpdateApcAction;
use Paneric\ComponentModuleApc\Interfaces\Service\GetUpdateApcServiceInterface;

class GetUpdateApcService implements GetUpdateApcServiceInterface
{
    public function __construct(
        protected GetOneByIdApcAction $getOneByIdApcAction,
        protected UpdateApcAction $updateApcAction
    ) {
    }

    public function __invoke(
        string $id,
        string $method,
        string $proxyPrefix,
        ?array $attributes = null,
        ?string $token = null,
        ?string $local = null
    ): ?array {
        if ($method === 'GET') {
            return ($this->getOneByIdApcAction)($id, $token, $local);
        }

        $data = ($this->updateApcAction)($id, $proxyPrefix, $attributes, $token, $local);

        if ($data['status'] === 303) {
            return $data;
        }

        return array_merge(
            $data,
            ['item_content' => ($this->getOneByIdApcAction)($id, $token, $local)]
        );
    }
}
