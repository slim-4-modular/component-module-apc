<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Service;

use Paneric\ComponentModuleApc\Action\DeleteApcAction;
use Paneric\ComponentModuleApc\Action\GetOneByIdApcAction;
use Paneric\ComponentModuleApc\Interfaces\Service\GetDeleteApcServiceInterface;

class GetDeleteApcService implements GetDeleteApcServiceInterface
{
    public function __construct(
        protected GetOneByIdApcAction $getOneByIdApcAction,
        protected DeleteApcAction $deleteApcAction
    ) {
    }

    public function __invoke(
        string $id,
        string $method,
        string $proxyPrefix,
        ?string $token = null,
        ?string $local = null
    ): ?array {
        if ($method === 'GET') {
            return ($this->getOneByIdApcAction)($id, $token, $local);
        }

        $data = ($this->deleteApcAction)($id, $proxyPrefix, $token, $local);

        if ($data['status'] === 303) {
            return $data;
        }

        return array_merge(
            $data,
            ['item_content' => ($this->getOneByIdApcAction)($id, $token, $local)]
        );
    }
}
