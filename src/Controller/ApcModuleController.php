<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Controller;

use Paneric\ComponentModuleApc\Interfaces\Action\CreateApcActionInterface;
use Paneric\ComponentModuleApc\Interfaces\Action\CreateMultipleApcActionInterface;
use Paneric\ComponentModuleApc\Interfaces\Action\GetAllApcActionInterface;
use Paneric\ComponentModuleApc\Interfaces\Action\GetAllByApcActionInterface;
use Paneric\ComponentModuleApc\Interfaces\Action\GetAllByExtendedApcActionInterface;
use Paneric\ComponentModuleApc\Interfaces\Action\GetAllPaginatedApcActionInterface;
use Paneric\ComponentModuleApc\Interfaces\Action\GetOneByApcActionInterface;
use Paneric\ComponentModuleApc\Interfaces\Action\GetOneByIdApcActionInterface;
use Paneric\ComponentModuleApc\Interfaces\Service\GetDeleteApcServiceInterface;
use Paneric\ComponentModuleApc\Interfaces\Service\GetDeleteMultipleApcServiceInterface;
use Paneric\ComponentModuleApc\Interfaces\Service\GetUpdateApcServiceInterface;
use Paneric\ComponentModuleApc\Interfaces\Service\GetUpdateMultipleApcServiceInterface;
use Paneric\Interfaces\Responder\ResponderInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class ApcModuleController
{
    public function __construct(readonly protected ResponderInterface $responder)
    {
    }

    public function create(
        Request $request,
        Response $response,
        CreateApcActionInterface $action
    ): Response {
        return $this->c(
            $request,
            $response,
            $action,
            'create',
            'add-update'
        );
    }

    public function createMultiple(
        Request $request,
        Response $response,
        CreateMultipleApcActionInterface $action
    ): Response {
        return $this->c(
            $request,
            $response,
            $action,
            'createMultiple',
            'adds-updates'
        );
    }

    public function delete(
        Request $request,
        Response $response,
        GetDeleteApcServiceInterface $service,
        string $id
    ): Response {
        $data = $service(
            $id,
            $request->getMethod(),
            $request->getAttribute('proxy_prefix'),
            $request->getAttribute('token'),
            $request->getAttribute('local'),
        );

        if ($data['status'] === 303) {
            return ($this->responder)($response, $data, null);
        }

        return ($this->responder)(
            $response,
            $data,
            sprintf('@%s/delete.html.twig', $request->getAttribute('proxy_scope'))
        );
    }

    public function deleteMultiple(
        Request $request,
        Response $response,
        GetDeleteMultipleApcServiceInterface $service,
        string $page
    ): Response {
        $data = $service(
            $page,
            $request->getMethod(),
            $request->getAttribute('proxy_prefix'),
            $request->getQueryParams(),
            $request->getParsedBody(),
            $request->getAttribute('token'),
            $request->getAttribute('local'),
        );

        if ($data['status'] === 303) {
            return ($this->responder)($response, $data, null);
        }

        return ($this->responder)(
            $response,
            $data,
            sprintf('@%s/deletes.html.twig', $request->getAttribute('proxy_scope'))
        );
    }

    public function getAll(
        Request $request,
        Response $response,
        GetAllApcActionInterface $action
    ): Response {
        return ($this->responder)(
            $response,
            $action(
                $request->getAttribute('token'),
                $request->getQueryParams(),
                $request->getAttribute('local')
            ),
            sprintf('@%s/get-all.html.twig', $request->getAttribute('proxy_scope'))
        );
    }

    public function getAllBy(
        Request $request,
        Response $response,
        GetAllByApcActionInterface $action,
        string $field,
        string $value
    ): Response {
        return ($this->responder)(
            $response,
            $action(
                $field,
                $value,
                $request->getAttribute('token'),
                $request->getQueryParams(),
                $request->getAttribute('local')
            ),
            sprintf('@%s/get-all.html.twig', $request->getAttribute('proxy_scope'))
        );
    }

    public function getAllByExtended(
        Request $request,
        Response $response,
        GetAllByExtendedApcActionInterface $action,
    ): Response {
        return ($this->responder)(
            $response,
            $action(
                $request->getAttribute('token'),
                $request->getQueryParams(),
                $request->getAttribute('local')
            ),
            sprintf('@%s/get-all.html.twig', $request->getAttribute('proxy_scope'))
        );
    }

    public function getAllPaginated(
        Request $request,
        Response $response,
        GetAllPaginatedApcActionInterface $action,
        string $page
    ): Response {
        return ($this->responder)(
            $response,
            $action(
                $page,
                $request->getAttribute('token'),
                $request->getQueryParams(),
                $request->getAttribute('local')
            ),
            sprintf('@%s/get-all.html.twig', $request->getAttribute('proxy_scope'))
        );
    }

    public function getOneBy(
        Request $request,
        Response $response,
        GetOneByApcActionInterface $action,
        string $field,
        string $value
    ): Response {
        return ($this->responder)(
            $response,
            $action(
                $field,
                $value,
                $request->getAttribute('token'),
                $request->getAttribute('local')
            ),
            sprintf('@%s/get-one.html.twig', $request->getAttribute('proxy_scope'))
        );
    }

    public function getOneById(
        Request $request,
        Response $response,
        GetOneByIdApcActionInterface $action,
        string $id
    ): Response {
        return ($this->responder)(
            $response,
            $action(
                $id,
                $request->getAttribute('token'),
                $request->getAttribute('local')
            ),
            sprintf('@%s/get-one.html.twig', $request->getAttribute('proxy_scope'))
        );
    }

    public function update(
        Request $request,
        Response $response,
        GetUpdateApcServiceInterface $service,
        string $id
    ): Response {
        $data = $service(
            $id,
            $request->getMethod(),
            $request->getAttribute('proxy_prefix'),
            $request->getParsedBody(),
            $request->getAttribute('token'),
            $request->getAttribute('local'),
        );

        if ($data['status'] === 303) {
            return ($this->responder)($response, $data, null);
        }

        return ($this->responder)(
            $response,
            $data,
            sprintf('@%s/add-update.html.twig', $request->getAttribute('proxy_scope'))
        );
    }

    public function updateMultiple(
        Request $request,
        Response $response,
        GetUpdateMultipleApcServiceInterface $service,
        string $page
    ): Response {
        $data = $service(
            $page,
            $request->getMethod(),
            $request->getAttribute('proxy_prefix'),
            $request->getQueryParams(),
            $request->getParsedBody(),
            $request->getAttribute('token'),
            $request->getAttribute('local'),
        );

        if ($data['status'] === 303) {
            return ($this->responder)($response, $data, null);
        }

        return ($this->responder)(
            $response,
            $data,
            sprintf('@%s/adds-updates.html.twig', $request->getAttribute('proxy_scope'))
        );
    }

    protected function c(
        Request $request,
        Response $response,
        mixed $action,
        string $configMethod,
        string $templatePrefix
    ): Response {
        $data = $action(
            $request->getMethod(),
            $configMethod,
            $request->getAttribute('proxy_prefix'),
            $request->getParsedBody(),
            $request->getAttribute('token'),
            $request->getAttribute('local'),
        );

        if ($data['status'] === 303) {
            return ($this->responder)($response, $data, null);
        }

        return ($this->responder)(
            $response,
            $data,
            sprintf('@%s/%s.html.twig', $request->getAttribute('proxy_scope'), $templatePrefix)
        );
    }
}
