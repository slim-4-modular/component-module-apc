<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Interfaces;

interface ModuleConfigInterface
{
    public function create(
        string $prefix,
        ?array $attributes,
        ?string $token = null,
        ?string $local = null
    ): array;

    public function createMultiple(
        string $proxyPrefix,
        ?array $attributes,
        ?string $token = null,
        ?string $local = null
    ): array;

    public function delete(
        string $proxyPrefix,
        ?string $token = null,
        ?string $local = null
    ): array;

    public function deleteMultiple(
        string $proxyPrefix,
        ?array $attributes,
        ?string $token = null,
        ?string $local = null
    ): array;

    public function getAll(?string $token = null, ?array $queryParams = null, ?string $local = null): array;

    public function getAllBy(?string $token = null, ?array $queryParams = null, ?string $local = null): array;

    public function getAllByExtended(?string $token = null, ?array $queryParams = null, ?string $local = null): array;

    public function getAllPaginated(?string $token = null, ?array $queryParams = null, ?string $local = null): array;

    public function getOneBy(?string $token = null, ?string $local = null): array;

    public function getOneById(?string $token = null, ?string $local = null): array;

    public function update(
        string $prefix,
        ?array $attributes,
        ?string $token = null,
        ?string $local = null
    ): array;

    public function updateMultiple(
        string $proxyPrefix,
        ?array $attributes,
        ?string $token = null,
        ?string $local = null
    ): array;

    public function getApiBaseUrl(): string;

    public function getModuleName(): string;
}
