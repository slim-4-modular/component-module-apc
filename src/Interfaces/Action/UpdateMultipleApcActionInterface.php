<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Interfaces\Action;

interface UpdateMultipleApcActionInterface
{
    public function __invoke(
        string $proxyPrefix,
        ?array $attributes = null,
        ?string $token = null,
        ?string $local = null
    ): ?array;
}
