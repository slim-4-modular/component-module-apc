<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Interfaces\Action;

interface CreateApcActionInterface
{
    public function __invoke(
        string $method,
        string $configMethod,
        string $proxyPrefix,
        ?array $attributes = null,
        ?string $token = null,
        ?string $local = null
    ): ?array;
}
