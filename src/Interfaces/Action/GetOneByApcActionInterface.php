<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Interfaces\Action;

interface GetOneByApcActionInterface
{
    public function __invoke(string $field, string $value, ?string $token = null, ?string $local = null): array;
}
