<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Interfaces\Action;

interface GetAllPaginatedApcActionInterface
{
    public function __invoke(
        string $page,
        ?string $token = null,
        ?array $queryParams = null,
        ?string $local = null
    ): array;
}
