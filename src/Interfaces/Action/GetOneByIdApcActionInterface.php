<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Interfaces\Action;

interface GetOneByIdApcActionInterface
{
    public function __invoke(string $id, ?string $token = null, ?string $local = null): array;
}
