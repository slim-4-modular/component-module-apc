<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Interfaces\Action;

interface GetAllApcActionInterface
{
    public function __invoke(?string $token = null, ?array $queryParams = null, ?string $local = null): array;
}
