<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Interfaces\Action;

interface GetAllByApcActionInterface
{
    public function __invoke(
        string $field,
        string $value,
        ?string $token = null,
        ?array $queryParams = null,
        ?string $local = null
    ): array;
}
