<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Interfaces\Action;

interface DeleteApcActionInterface
{
    public function __invoke(
        $id,
        string $proxyPrefix,
        ?string $token = null,
        ?string $local = null
    ): ?array;
}
