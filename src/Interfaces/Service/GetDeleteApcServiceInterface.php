<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Interfaces\Service;

interface GetDeleteApcServiceInterface
{
    public function __invoke(
        string $id,
        string $method,
        string $proxyPrefix,
        ?string $token = null,
        ?string $local = null
    ): ?array;
}
