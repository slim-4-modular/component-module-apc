<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Interfaces\Service;

interface GetUpdateMultipleApcServiceInterface
{
    public function __invoke(
        string $page,
        string $method,
        string $proxyPrefix,
        ?array $queryParams = null,
        ?array $attributes = null,
        ?string $token = null,
        ?string $local = null
    ): ?array;
}
