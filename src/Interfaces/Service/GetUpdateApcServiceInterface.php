<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApc\Interfaces\Service;

interface GetUpdateApcServiceInterface
{
    public function __invoke(
        string $id,
        string $method,
        string $proxyPrefix,
        ?array $attributes = null,
        ?string $token = null,
        ?string $local = null
    ): ?array;
}
